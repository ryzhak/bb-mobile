// Code goes here

var app = angular.module('bb-mobile', ['ionic', 'ngCordova']);

app.run(function($rootScope, $ionicPlatform){

    $rootScope.games = [];
    $rootScope.teams = [];

    //инициализируем GA
    $ionicPlatform.ready(function(){
        if(typeof analytics !== 'undefined'){
            analytics.startTrackerWithId('UA-55203985-2');
        }
    });

});

app.value('gamesUrl','http://www.bookiebeater.net/game/nearestgames');
//app.value('gamesUrl','http://localhost/bb/web/game/nearestgames');

app.value('teamsUrl','http://www.bookiebeater.net/game/getteams?seasonid=2');
//app.value('teamsUrl','http://localhost/bb/web/game/getteams?seasonid=2');

app.value('scoreUrl','http://www.bookiebeater.net/game/getscore');
//app.value('scoreUrl','http://localhost/bb/web/game/getscore');

app.value('betsUrl','http://www.bookiebeater.net/game/getbets');
//app.value('betsUrl','http://localhost/bb/web/game/getbets');

app.value('params', {
    hoursMinus: 2,
    hoursPlus: 24,
    timezoneOffset: 0, //London
    timezoneIndex: 39,
    timeoutLoadingGames: 20000,
    timeoutLoadingScore: 15000,
    timeoutLoadingBets: 15000
});

app.config(function($stateProvider, $urlRouterProvider){

    $urlRouterProvider.otherwise('/games');

    $stateProvider.state('stream', {
        abstract: true,
        templateUrl: 'main.html'
    });

    $stateProvider.state('stream.games', {
        abstract: true,
        url: '/games',
        views: {
            games: {
                template: "<ion-nav-view animation='none'></ion-nav-view>"
            }
        }
    });

    $stateProvider.state('stream.games.index', {
        url: '',
        templateUrl: 'games.html',
        controller: 'GamesCtrl'
    });

    $stateProvider.state('stream.games.detail', {
        url: '/:gameID',
        templateUrl: 'game.html',
        controller: 'GameCtrl',
        resolve: {
            gameM: function($stateParams, GamesService){
                return GamesService.getGame($stateParams.gameID);
            }
        }
    });

    $stateProvider.state('stream.predictor', {
        url: '/predictor',
        views: {
            predictor: {
                templateUrl:"predictor.html",
                controller: 'PredictorCtrl'
            }
        }
    });

});