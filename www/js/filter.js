app.filter('nospace', function(){
    return function(value){
        return (!value) ? '' : value.replace(/ /g,'') ;
    };
});

app.filter('lowercase', function(){
    return function(value){
        return (!value) ? '' : value.toLowerCase() ;
    };
});

app.filter('asDate', function(){
    return function(value){
        return Date.parse(value);
    };
});

app.filter('dateToTimezone', function(){
    return function(value, diffHours){
        return value + diffHours * 60 * 60 * 1000;
    };
});

app.filter('toAceStreamFromLiveTV', function(){
    return function(value){
        var start = value.indexOf('&c=');
        var end = value.indexOf('&lang=');
        return 'acestream://' + value.substring(start + 3, end);
    };
});

app.filter('gmt', function(){
    return function(value){
        switch(value){
            case -4.5: return 'GMT' + '-4:30';
            case -3.5: return 'GMT' + '-3:30';
            case 3.5: return 'GMT' + '+3:30';
            case 5.5: return 'GMT' + '+5:30';
            case 5.75: return 'GMT' + '+5:45';
            case 6.5: return 'GMT' + '+6:30';
            case 9.5: return 'GMT' + '+9:30';
            case 10.5: return 'GMT' + '+10:30';
        }
        if(value >= 0){
            return 'GMT+' + value + ':00';
        } else {
            return 'GMT' + value + ':00';
        }
    };
});