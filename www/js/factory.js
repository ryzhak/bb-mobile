app.factory("GamesService", function($http, gamesUrl, params){

    var games = [];

    $http.get(gamesUrl, {
        params: {
            hoursMinus: params.hoursMinus,
            hoursPlus: params.hoursPlus
        },
        timeout: params.timeoutLoadingGames
    }).then(function(resp){
        games = resp.data;
    }, function(err){
        console.log('Error loading games',err);
    });

    return {
        games: games,
        getGame: function(index){
            return games[index];
        }
    };
});

app.factory('ClubsService', function($http, teamsUrl){

    return {
        getTeams: function(){
            return $http.get(teamsUrl);
        }
    };

});

app.factory('StringService', function(){
    return {
        /**
         * Возвращает название страны, из England - Premier League вернет England
         *
         * @param name
         * @return string название страны
         */
        getCountryName: function(name){
            var blankSymb = name.indexOf(' ');
            return name.substring(0, blankSymb);
        },
        getLeagueName: function(name){
            var hyphenSymb = name.indexOf('-');
            return name.substring(hyphenSymb + 1);
        }
    };
});

app.factory('$localStorage', function($window){
    return {
        set: function(key, value){
            $window.localStorage[key] = value;
        },
        get: function(key, defaultValue){
            return $window.localStorage[key] || defaultValue;
        },
        setObject: function(key, value){
            $window.localStorage[key] = JSON.stringify(value);
        },
        getObject: function(key){
            return JSON.parse($window.localStorage[key] || '{}');
        }
    };
});
