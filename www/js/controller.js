app.controller('GamesCtrl', function($scope, GamesService, $http, gamesUrl, $ionicLoading, $filter, $rootScope, $interval,
                                     ClubsService, params, $ionicModal, timezones, $localStorage, $ionicPopup, $location,
                                     $window){

    $ionicModal.fromTemplateUrl('modal-timezone.html', {
        scope: $scope,
        animation: 'none'
    }).then(function(modal){
        $scope.modalTimezone = modal;
    });

    //если нет игр то делаем запрос к серваку
    if(!$rootScope.games.length){

        $ionicLoading.show({
            template: 'Loading...'
        });

        $http.get(gamesUrl, {
            params: {
                hoursMinus: params.hoursMinus,
                hoursPlus: params.hoursPlus
            },
            timeout: params.timeoutLoadingGames
        }).then(function(resp){

            $scope.games = resp.data;
            $rootScope.games = $scope.games;

            //запускаем этот запрос(обновление ссылок) выполняться каждые 3 минуты
            $interval(function(){
                $http.get(gamesUrl, {
                    params: {
                        hoursMinus: params.hoursMinus,
                        hoursPlus: params.hoursPlus
                    }
                }).then(function(resp){
                    $scope.games = resp.data;
                    $rootScope.games = $scope.games;
                }, function(err){
                    console.log('ERR', err);
                });
            }, 3 * 60 * 1000);

            //получаем все команды для страницы predictor
            ClubsService.getTeams().then(function(resp){

                    $rootScope.teams = resp.data;
                    $ionicLoading.hide();

                }, function(err){
                    console.log('ERR', err);}
            );

        }, function(err){

            //при ошибки загрузки игр показываем окно с просьбой попробовать еще раз

            console.log('Error loading games',err);
            $ionicLoading.hide();
            //показваем popup
            var alertPopup = $ionicPopup.alert({
                title: 'No internet connection',
                okText: 'Try again'
            });
            alertPopup.then(function(res){

                $location.path('/games');
                $window.location.reload();
                $ionicLoading.show({
                    template: 'Loading...'
                });

            });
        });
    } else {
        $scope.games = $rootScope.games;
    }

    //показываем лейбл лайв
    $scope.showLiveLabel = function(index){
        var gameTime = $filter('dateToTimezone')($filter('asDate')($scope.games[index].gameDate), $scope.timezoneOffset);
        var currentTime = new Date().getTime();
        var plus2HoursTime = gameTime + 2 * 60 * 60 * 1000;
        return currentTime > gameTime && currentTime < plus2HoursTime;
    };

    //показываем настройки таймзоны
    $scope.openTimezoneModal = function(){
        $scope.modalTimezone.show();
    };

    $scope.closeTimezoneModal = function(){
        $scope.modalTimezone.hide();
    };

    $scope.timezones = timezones;

    $scope.selectTimezone = function(index){
        $scope.timezoneOffset = $scope.timezones[index].offset;
        $localStorage.set('timezoneOffset', $scope.timezones[index].offset);
        $localStorage.set('timezoneIndex', index);
        $scope.timezoneIndex = index;
        $scope.modalTimezone.hide();
    };

    $scope.timezoneOffset = $localStorage.get('timezoneOffset' ,params.timezoneOffset);

    $scope.timezoneIndex = $localStorage.get('timezoneIndex' ,params.timezoneIndex);

});

app.controller("GameCtrl", function($scope, $http, gameM, $filter, $cordovaSocialSharing){

    $scope.game = gameM;

    $scope.openLink = function(link){

        //если торрент ссылка с лайв тв
        if(link.indexOf('livetv') >= 0 && link.indexOf('acestream') >= 0){
            link = $filter('toAceStreamFromLiveTV')(link);
        }
        console.log(link);
        window.open(link, '_system');

    };

    $scope.share = function(){
        var link = 'http://www.bookiebeater.net/stream/view?id=' + $scope.game.id;
        var mes = $scope.game.teamHome + ' vs ' + $scope.game.teamAway + ' online ';
        var champ = $scope.game.championship;
        $cordovaSocialSharing.share(mes, champ, null, link);
    };


});

app.controller('PredictorCtrl', function($scope, ClubsService,
                                         $ionicSideMenuDelegate,
                                         StringService,
                                         $http,
                                         scoreUrl,
                                         betsUrl,
                                         $filter,
                                         $rootScope,
                                         params,
                                         $ionicPopup,
                                         $timeout, $ionicNavBarDelegate){

    $timeout(function(){
        $ionicNavBarDelegate.showBackButton(false);
    },5);

    $scope.selects = {};
    $scope.showPredBlock = false;
    $scope.showGetPredictButton = true;
    $scope.curLeagueIndex = 4; //Англия по дефолту
    $scope.homeImgPath = 'img/other/no_image.png';
    $scope.awayImgPath = 'img/other/no_image.png';

    //когда получены все данные то показывает вьюху
    $scope.showPredictLoader = false;

    $scope.teams = $rootScope.teams;
    //console.log($scope.teams);

    if($scope.teams.length){
        $scope.leagueTeams = $scope.teams[$scope.curLeagueIndex].teams;
    }
    //console.log($scope.leagueTeams);

    $scope.getCountryName = StringService.getCountryName;

    $scope.getLeagueName = StringService.getLeagueName;

    //при изменении команды очищает блок с прогнозом
    $scope.clearPredictorView = function(){
        $scope.showPredBlock = false;
        $scope.showGetPredictButton = true;
    };

    //при изменении лиги в левом меню
    $scope.changeTeams = function(index){
        $scope.leagueTeams = $scope.teams[index].teams;
        $scope.curLeagueIndex = index;
        $scope.clearPredictorView();
        $ionicSideMenuDelegate.toggleLeft();
    };

    $scope.toggleLeft = function(){
        $ionicSideMenuDelegate.toggleLeft();
    };

    //нажатие на кнопку "get prediction"
    $scope.showPrediction = function(){

        //проверка на пустые id команд
        if($scope.selects.teamHome == null || $scope.selects.teamAway == null){
            return;
        }
        //проверка на одинаковые id команд
        if($scope.selects.teamHome.id == $scope.selects.teamAway.id){
            return;
        }

        $scope.showPredictLoader = true;

        //получаем счет
        $http.get(scoreUrl, {
            params: {
                teamhomeid: $scope.selects.teamHome.id,
                teamawayid: $scope.selects.teamAway.id
            },
            timeout: params.timeoutLoadingScore
        }).then(function(resp){

            $scope.teamHomeGoals = resp.data.goalsHome;
            $scope.teamAwayGoals = resp.data.goalsAway;

            //получаем прогнозы
            $http.get(betsUrl, {
                params: {
                    teamhomeid: $scope.selects.teamHome.id,
                    teamawayid: $scope.selects.teamAway.id
                },
                timeout: params.timeoutLoadingBets
            }).then(function(resp){

                //получаем только первые 3 ставки
                $scope.bets = resp.data.slice(0,3);

                $scope.homeImgPath = 'img/clubs/'
                + $filter('lowercase')(StringService.getCountryName($scope.teams[$scope.curLeagueIndex].league))
                + '/' + $filter('nospace')($filter('lowercase')($scope.selects.teamHome.name)) + '.png';

                $scope.awayImgPath = 'img/clubs/'
                + $filter('lowercase')(StringService.getCountryName($scope.teams[$scope.curLeagueIndex].league))
                + '/' + $filter('nospace')($filter('lowercase')($scope.selects.teamAway.name)) + '.png';

                //выключаем лоадер на кнопке и саму кнопку
                $scope.showPredictLoader = false;
                $scope.showGetPredictButton = false;

                //показываем блок
                $scope.showPredBlock = true;

            }, function(err){
                $scope.showPredictLoader = false;
                var alertPopup = $ionicPopup.alert({
                    title: 'Failed to load bets',
                    okText: 'Close'
                });
            });


        }, function(err){
            console.log('Can not load score', err);
            $scope.showPredictLoader = false;
            var alertPopup = $ionicPopup.alert({
                title: 'Failed to load scores',
                okText: 'Close'
            });
        });

    };

});
